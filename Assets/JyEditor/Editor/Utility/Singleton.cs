﻿using System;

namespace JyEditor
{
    /// <summary>
    /// 简单单件
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Singleton<T> where T : new()
    {
        /// <summary>
        /// 单件实例
        /// </summary>
        private static T _singleton = default(T);

        /// <summary>
        /// 安全锁
        /// </summary>
        private static object _safelock = new object();

        /// <summary>
        /// 构造
        /// </summary>
        protected Singleton()
        {
            Init();
        }

        /// <summary>
        /// 初始化
        /// </summary>
        protected virtual void Init()
        {

        }

        /// <summary>
        /// 单件对象
        /// </summary>
        public static T Ins
        {
            get
            {
                if (_singleton == null)
                {
                    lock (_safelock)
                    {
                        if (_singleton == null) _singleton = new T();
                    }
                }
                return _singleton;
            }
        }
    }
}
﻿/*
 *  date: 2018-03-27
 *  author: John-chen
 *  cn: 资源管理模块
 *  en: todo:
 */

using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// 资源管理模块
    /// </summary>
    public class ResourceModule : BaseModule
    {
        /// <summary>
        /// 加载器
        /// </summary>
        public ResLoader Loader { get { return _loader; } }

        public ResourceModule(string name = ModuleName.Res) : base(name)
        {

        }

        /// <summary>
        /// 初始化
        /// </summary>
        public override void InitGame()
        {
            // 引用管理器
            RefController.CreateSingleton();

            // 创建加载器
            _loader = new ResLoader();
        }

        public override void UpdateGame(float deltaTime)
        {
            //Debug.Log("Res Module update!");
        }

        public override void ExitGame()
        {
            _loader.Remove();
            RefController.Ins.Remove();
        }

        protected ResLoader _loader;
    }
}

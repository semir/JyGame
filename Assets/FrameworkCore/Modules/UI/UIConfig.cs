﻿/*
 *  date: 2017-07-24
 *  author: John-Chen
 *  cn: UI的配置信息
 *  en: todo:
 */

using System;
using GameCore;
using System.Collections.Generic;

namespace JyFramework
{
    /// <summary>
    /// 使用的UI框架
    /// </summary>
    public enum UIFKType
    {
        NGUI,
        UGUI,
    }

    /// <summary>
    /// UI类型与路径的配置信息
    /// todo: 后面会读表配置
    /// </summary>
    public sealed class UIConfig
    {
        /// <summary>
        /// 这里做更改使用的UI框架
        /// </summary>
        public static UIFKType UIFKType { get {return UIFKType.UGUI;} }

        /// <summary>
        /// 配置UI的信息
        /// </summary>
        public static void ConfigPath()
        {
            if(_allWndsPrefabPath == null) _allWndsPrefabPath = new Dictionary<Type, WindowInfo>();

            AddPath<TestWnd>("TestStartWindow.prefab", WindowType.FullScreen, WindowCacheTime.Cache0);
            AddPath<TestWnd2>("TestStartWindow2.prefab", WindowType.FullScreen, WindowCacheTime.Cache0);
        }

        /// <summary>
        /// 获取UI的资源路径
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static WindowInfo GetPath<T>() where T : BaseWindow
        {
            var type = typeof(T);
            if (_allWndsPrefabPath != null && _allWndsPrefabPath.ContainsKey(type))
                return _allWndsPrefabPath[type];

            return null;
        }

        /// <summary>
        /// 添加UI信息到配置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        private static void AddPath<T>(string path, WindowType wndType, WindowCacheTime cacheTime) where T : BaseWindow
        {
            var type = typeof(T);
            var name = GetName(type);
            WindowInfo info = new WindowInfo(wndType, cacheTime, path, name);

            if (!_allWndsPrefabPath.ContainsKey(type))
            {
                _allWndsPrefabPath.Add(type, info);
            }
            else
            {
                _allWndsPrefabPath[type] = info;
            }
        }

        /// <summary>
        /// 通过类型获取名字
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string GetName(Type type)
        {
            string nspace = "GameCore.";
            var myName = type.ToString();

            string name = myName;
            if (myName.Contains(nspace))
            {
                name = myName.Substring(nspace.Length);
            }
            
            return name;
        }

        private static Dictionary<Type, WindowInfo> _allWndsPrefabPath;
    }


    /// <summary>
    /// 传递UI事件的参数
    /// </summary>
    public class UIClassType
    {
        /// <summary>
        /// UI的类型
        /// </summary>
        public Type UIType { get { return _type;} }

        /// <summary>
        /// 窗口信息
        /// </summary>
        public BaseWindow Wnd { get { return _wnd;} }

        public UIClassType(Type type, BaseWindow wnd)
        {
            _type = type;
            _wnd = wnd;
        }

        private Type _type;
        private BaseWindow _wnd;
    }
}
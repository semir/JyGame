﻿/*
 *  date: 2018-07-24
 *  author: John-chen
 *  cn: UI操作得回调
 *  en: todo:
 */

using UnityEngine;
using System.Collections;

namespace JyFramework
{
    /// <summary>
    /// UI操作得回调
    /// </summary>
    public class WndAction
    {
        public WndAction(UIAction action, params object[] args)
        {
            _action = action;
            _args = args;
        }

        /// <summary>
        /// 执行回调
        /// </summary>
        public void Execute()
        {
            _action?.Invoke(_args);
        }

        private UIAction _action;
        private object[] _args;

    }
}
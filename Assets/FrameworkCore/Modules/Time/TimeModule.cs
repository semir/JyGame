﻿/*
 *  date: 2018-03-27
 *  author: John-chen
 *  cn: 时间模块
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 时间模块
    /// </summary>
    public class TimeModule : BaseModule
    {
        public TimeModule(string name = ModuleName.Time) : base(name)
        {

        }

        public override void InitGame()
        {
            
        }
    }
}

﻿/*
 * date: 2018-10-09
 * author: John-chen
 * cn: 日志打印类
 * en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 日志打印类
    /// </summary>
    public class JyLog
    {
        public JyLog()
        {
            Ctor();
        }

        /// <summary>
        /// 是否保存到客户端
        /// </summary>
        public bool IsSaveToClient { get; set; }

        /// <summary>
        /// 是否发送到服务端
        /// </summary>
        public bool IsSendToServer { get; set; }

        /// <summary>
        /// 是否在editor上显示
        /// </summary>
        public bool IsShowEditor { get; set; }

        /// <summary>
        /// 设置打印委托
        /// </summary>
        /// <param name="logDelegate"></param>
        public void SetLogDelegate(LogDelegate logDelegate)
        {
            _logDelegate = logDelegate;
        }

        /// <summary>
        /// Info级打印
        /// </summary>
        /// <param name="logInfo"> 日志内容 </param>
        public void Info(object logInfo)
        {
            LogPrint(logInfo, LogLevel.Info);
            SaveLogInfo(logInfo, LogLevel.Error);
        }

        /// <summary>
        /// Warning级打印
        /// </summary>
        /// <param name="logInfo"></param>
        public void Warning(object logInfo)
        {
            LogPrint(logInfo, LogLevel.Warning);
            SaveLogInfo(logInfo, LogLevel.Error);
        }

        /// <summary>
        /// Error级打印
        /// </summary>
        /// <param name="logInfo"></param>
        public void Error(object logInfo)
        {
            LogPrint(logInfo, LogLevel.Error);
            SaveLogInfo(logInfo, LogLevel.Error);
        }

        /// <summary>
        /// log打印
        /// </summary>
        /// <param name="logInfo"> 日志内容 </param>
        /// <param name="lv"> 输出级别 </param>
        private void LogPrint(object logInfo, LogLevel lv)
        {
            if(!IsShowEditor) return;
            _logDelegate?.Invoke(logInfo, lv);
        }

        /// <summary>
        /// 保存log日志信息
        /// </summary>
        /// <param name="logInfo"></param>
        /// <param name="lv"></param>
        private void SaveLogInfo(object logInfo, LogLevel lv)
        {
            SaveToClient(logInfo, lv);
            SendToServer(logInfo, lv);
        }

        /// <summary>
        /// 保存到客户端
        /// </summary>
        /// <param name="logInfo"></param>
        /// <param name="lv"></param>
        private void SaveToClient(object logInfo, LogLevel lv)
        {
            if (!IsSaveToClient) return;
        }

        /// <summary>
        /// 上传到服务端
        /// </summary>
        /// <param name="logInfo"></param>
        /// <param name="lv"></param>
        private void SendToServer(object logInfo, LogLevel lv)
        {
            if(!IsSendToServer) return;
        }

        private void Ctor()
        {
            IsShowEditor = true;
        }

        private LogDelegate _logDelegate;
    }
}
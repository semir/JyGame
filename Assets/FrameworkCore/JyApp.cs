﻿/*
 *  date: 2018-03-27
 *  author: John-chen
 *  cn: 游戏框架启动器
 *  en: todo:
 */

using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// 游戏框架启动器
    /// </summary>
    public class JyApp : BaseApp
    {
        public static GameObject GameApp { get; protected set; }

        public static JyApp Ins { get { return _ins; } }

        public static JyApp CreateInstance(GameObject gameApp, string name = "")
        {
            if(_ins == null) _ins = new JyApp(gameApp, name);

            return _ins;
        }

        public JyApp(GameObject gameApp, string name = "JyGame"):base(name)
        {
            GameApp = gameApp;
            Debug.Log("JyApp Ctor----------------");
        }

        /// <summary>
        /// 读取配置文件
        /// </summary>
        protected override void ReadConfig()
        {
            
            Debug.Log("JyApp ReadConfig----------------");
        }

        /// <summary>
        /// 初始化dll
        /// </summary>
        protected override void InitDlls()
        {
            Debug.Log("JyApp InitDlls----------------");
        }

        /// <summary>
        /// 添加所有模块
        /// todo: 最后可能会通过读取的配置文件，反射初始化模块
        /// </summary>
        protected override void AddModules()
        {
            Debug.Log("JyApp AddModules----------------");
            // 有顺序的添加和初始化模块
            AddModule(ModuleName.Log,        new LogModule()         );         // 日志
            AddModule(ModuleName.Util,       new UtilModule()        );         // 工具
            AddModule(ModuleName.Time,       new TimeModule()        );         // 时间
            AddModule(ModuleName.Data,       new DataModule()        );         // 数据
            AddModule(ModuleName.Event,      new EventModule()       );         // 事件
            AddModule(ModuleName.Thread,     new ThreadModule()      );         // 线程
            AddModule(ModuleName.Script,     new ScriptModule()      );         // 脚本

            AddModule(ModuleName.Network,    new NetworkModule()     );         // 网络
            AddModule(ModuleName.Res,        new ResourceModule()    );         // 资源
            AddModule(ModuleName.UI,         new UIModule()          );         // UI

            AddModule(ModuleName.Aduio,      new AudioModule()       );         // 音频
            AddModule(ModuleName.Object,     new ObjectModule()      );         // 对象池
            AddModule(ModuleName.AI,         new AIModule()          );         // AI
        }

        protected static JyApp _ins;
    }
}